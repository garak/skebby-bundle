<?php

namespace Zen\Bundle\SkebbyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('skebby');

        $rootNode->children()
            ->scalarNode('url')
                ->defaultValue('http://gateway.skebby.it/api/send/smseasy/advanced/http.php')
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('username')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('password')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->booleanNode('test_mode')
                ->defaultValue(false)
            ->end()
            ->scalarNode('add_prefix')
            ->end()
            ->scalarNode('clean_regexp')
            ->end()
         ->end()
        ;

        return $treeBuilder;
    }
}
