<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\Bundle\SkebbyBundle\Util\Skebby;

class SkebbyBasicSMSCommand extends AbstractSkebbySMSCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
                ->setName('skebby:sms:basic')
                ->setDescription('Send simple sms')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $skebby = $this->getSkebby();
        $phones = $input->getOption('phone');

        $output->writeln('');
        $output->writeln('Send sms to phones: <comment>'.implode(', ', $phones).'</comment>');
        $output->writeln('');

        $result = $skebby->sendSMS($phones, $input->getArgument('message'), Skebby::SMS_TYPE_CLASSIC);
        if (!$skebby->isResultError($result)) {
            $output->writeln('<info>OK</info>');
        } else {
            $output->writeln(sprintf('Error: <error>%s</error>', $skebby->getResultErrorMessage($result)));
        }

        $output->writeln('');
    }
}
