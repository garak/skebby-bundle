<?php

namespace Zen\Bundle\SkebbyBundle\EventListener;

use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;

class AddPrefixListener
{
    private $prefix;
    private $checkExistence;

    /**
     * @param string $prefix
     * @param bool   $checkExistence
     */
    public function __construct($prefix = '39', $checkExistence = false)
    {
        $this->prefix = $prefix;
        $this->checkExistence = $checkExistence;
    }

    /**
     * Set international prefix.
     *
     * @param string $prefix
     *
     * @return \Zen\Bundle\SkebbyBundle\EventListener\AddPrefixListener
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get international prefix.
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    public function addPrefix(PreSendEvent $event)
    {
        $phones = $event->getPhones();

        if ($this->checkExistence) {
            $fn = function (&$phone, $key, $prefix) {
                if (0 !== strpos($phone, $prefix)) {
                    $phone = $prefix.$phone;
                }
            };
        } else {
            $fn = function (&$phone, $key, $prefix) {
                $phone = $prefix.$phone;
            };
        }

        array_walk($phones, $fn, $this->prefix);

        $event->setPhones($phones);
    }
}
