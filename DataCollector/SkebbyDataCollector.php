<?php

namespace Zen\Bundle\SkebbyBundle\DataCollector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Zen\Bundle\SkebbyBundle\Lib\SkebbyManager;

class SkebbyDataCollector extends DataCollector
{
    /**
     * @var SkebbyManager
     */
    private $skebby;

    public function __construct(SkebbyManager $skebby)
    {
        $this->skebby = $skebby;
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = $this->skebby->getDataCollector();
    }

    public function getData()
    {
        return $this->data;
    }

    public function getRequest()
    {
        return count($this->data);
    }

    public function getName()
    {
        return 'skebby';
    }

    public function reset()
    {
    }
}
