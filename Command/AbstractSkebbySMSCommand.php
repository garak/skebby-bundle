<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

abstract class AbstractSkebbySMSCommand extends AbstractSkebbyCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('message', InputArgument::REQUIRED)
            ->addOption('phone', 'p', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Phone number')
        ;
    }
}
